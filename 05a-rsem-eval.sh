#!/bin/bash
#SBATCH -t 6:00:00
#SBATCH -c 20
#SBATCH --mem=120G
#SBATCH -C e5-2600
# detonate 1.10
# bowtie 1.1.2
# samsool 1.2
set -e
source ./accessions.sh
SEED=3179812200
DIR=$PWD
for id in $IDS
do
	FQ1=$DIR/remove-rrna-$id/remove_rrna-*/reads.norrna.0.fq
	FQ2=$DIR/remove-rrna-$id/remove_rrna-*/reads.norrna.1.fq
	mkdir -p $DIR/rsem-eval-$id
	cd $DIR/rsem-eval-$id
	ln -s ../trinity-${id}/Trinity.fasta .
	rsem-eval-calculate-score -p 20 --output-bam --paired-end --seed $SEED $FQ1 $FQ2 Trinity.fasta rsem-eval-$id 392
done
