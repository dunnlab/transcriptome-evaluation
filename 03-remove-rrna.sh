#!/bin/bash
#SBATCH -t 24:00:00
#SBATCH -c 20
#SBATCH --mem=124G
#SBATCH -C e5-2600
# Trinity r20140717
# agalma/e3a2aa4
# biolite/0340611
set -e
module load trinity/r20140717
source ./accessions.sh
DIR=$PWD
export AGALMA_DB=$DIR/agalma.sqlite
export BIOLITE_RESOURCES="threads=20,memory=110G"
for id in $IDS
do
	mkdir -p $DIR/remove-rrna-$id
	cd $DIR/remove-rrna-$id
	agalma remove_rrna -i $id -f $DIR/${id}_1.fastq $DIR/${id}_2.fastq -o $DIR/remove-rrna-$id
done
