#!/bin/bash
#SBATCH -t 2:00:00
# sratoolkit 2.5.4
set -e
source ./accessions.sh
for id in $IDS
do
	fastq-dump --split-files --defline-seq '@$ac.$si/$ri' --defline-qual '+' $id
done
