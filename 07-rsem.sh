#!/bin/bash
#SBATCH -t 6:00:00
#SBATCH -c 20
#SBATCH --mem=120G
#SBATCH -C e5-2600
# rsem 1.2.22
# bowtie 1.1.2
# samsool 1.2
set -e
source ./accessions.sh
SEED=3179812200
DIR=$PWD
for id in $IDS
do
	FQ1=$DIR/remove-rrna-$id/remove_rrna-*/reads.norrna.0.fq
	FQ2=$DIR/remove-rrna-$id/remove_rrna-*/reads.norrna.1.fq
	mkdir -p $DIR/rsem-$id
	cd $DIR/rsem-$id
	# filter the Trinity assembly
	python ../corset-filter-assembly.py ../corset-$id/clusters.txt ../trinity-${id}/Trinity.fasta >Trinity.fasta
	# use mapping from Corset
	awk '{print $2"\t"$1}' ../corset-$id/clusters.txt >corset.gene_map
	rsem-prepare-reference --bowtie --transcript-to-gene-map corset.gene_map Trinity.fasta Trinity
	rsem-calculate-expression -p 20 --paired-end --seed $SEED ../${id}_1.fastq ../${id}_2.fastq Trinity rsem-$id
done
