#!/bin/bash
#SBATCH -t 24:00:00
#SBATCH -c 20
#SBATCH --mem=120G
#SBATCH -C e5-2600
# blast/2.2.29+
# transdecoder/r2012-08-15
# agalma/e3a2aa4
# biolite/0340611
set -e
source ./accessions.sh
DIR=$PWD
export AGALMA_DB=$DIR/agalma.sqlite
for id in $IDS
do
	export BIOLITE_RESOURCES="threads=20,memory=110G,outdir=$DIR/postassemble-$id"
	mkdir -p $DIR/postassemble-$id
	cd $DIR/postassemble-$id
        agalma catalog insert --id $id --paths ../trinity-${id}/Trinity.fasta
	agalma postassemble --id $id --external
done
