#!/bin/bash
#SBATCH -t 24:00:00
#SBATCH -c 20
#SBATCH --mem=124G
#SBATCH -C e5-2600
# Trinity 2.1.1
# bowtie 1.1.2
set -e
source ./accessions.sh
for id in $IDS
do
	FQ1=$PWD/remove-rrna-$id/remove_rrna-*/reads.norrna.0.fq
	FQ2=$PWD/remove-rrna-$id/remove_rrna-*/reads.norrna.1.fq
	Trinity --seqType fq --left $FQ1 --right $FQ2 --CPU 20 --max_memory 110G --output $PWD/trinity-$id
done
