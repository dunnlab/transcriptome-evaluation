#!/bin/bash
#SBATCH -t 1:00:00
#SBATCH -c 1
#SBATCH --mem=20G
# corset 1.04
set -e
source ./accessions.sh
DIR=$PWD
for id in $IDS
do
	mkdir -p $DIR/corset-$id
	cd $DIR/corset-$id
	corset ../rsem-eval-$id/rsem-eval-$id.transcript.sorted.bam
done
