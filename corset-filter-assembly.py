from Bio import SeqIO
import sys

ids = set(line.partition('\t')[0] for line in open(sys.argv[1]))

for record in SeqIO.parse(sys.argv[2], "fasta"):
    if record.id in ids:
        SeqIO.write(record, sys.stdout, "fasta")
